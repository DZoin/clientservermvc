﻿using PersonalBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_Bootstrap3_3_1_LESS.Controllers
{
    public class HomeController : Controller
    {
        private BlogPostDbContext db = new BlogPostDbContext();
        public ActionResult Index()
        {
            return View(db.BlogPost.ToList().OrderByDescending(post => post.DateAdded));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}