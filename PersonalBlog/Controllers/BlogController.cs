﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PersonalBlog.Models;
using System.Drawing;

namespace PersonalBlog.Controllers
{
    public class BlogController : Controller
    {
        private BlogPostDbContext db = new BlogPostDbContext();

        // GET: /Blog/
        public ActionResult Index()
        {
            return View(db.BlogPost.ToList().OrderByDescending(post => post.DateAdded));
        }

        // GET: /Blog/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPostModel blogpostmodel = db.BlogPost.Find(id);
            if (blogpostmodel == null)
            {
                return HttpNotFound();
            }
            return View(blogpostmodel);
        }

        // GET: /Blog/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Blog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,ImagePath,Title,Content,DateAdded")] BlogPostModel blogpostmodel)
        {
            if (ModelState.IsValid)
            {
                blogpostmodel.DateAdded = DateTime.Now;
                db.BlogPost.Add(blogpostmodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(blogpostmodel);
        }

        // GET: /Blog/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPostModel blogpostmodel = db.BlogPost.Find(id);
            if (blogpostmodel == null)
            {
                return HttpNotFound();
            }
            return View(blogpostmodel);
        }

        // POST: /Blog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,ImagePath,Title,Content")] BlogPostModel blogpostmodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blogpostmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blogpostmodel);
        }

        // GET: /Blog/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPostModel blogpostmodel = db.BlogPost.Find(id);
            if (blogpostmodel == null)
            {
                return HttpNotFound();
            }
            return View(blogpostmodel);
        }

        // POST: /Blog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BlogPostModel blogpostmodel = db.BlogPost.Find(id);
            db.BlogPost.Remove(blogpostmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
