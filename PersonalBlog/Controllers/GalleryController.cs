﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PersonalBlog.Models;

namespace PersonalBlog.Controllers
{
    public class GalleryController : Controller
    {
        private GalleryModelDbContext db = new GalleryModelDbContext();

        // GET: /Gallery/
        public async Task<ActionResult> Index()
        {
            return View(await db.Images.ToListAsync());
        }

        // GET: /Gallery/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GalleryModel gallerymodel = await db.Images.FindAsync(id);
            if (gallerymodel == null)
            {
                return HttpNotFound();
            }
            return View(gallerymodel);
        }

        // GET: /Gallery/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Gallery/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include="ID,ImageUrl")] GalleryModel gallerymodel)
        {
            if (ModelState.IsValid)
            {
                db.Images.Add(gallerymodel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(gallerymodel);
        }

        // GET: /Gallery/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GalleryModel gallerymodel = await db.Images.FindAsync(id);
            if (gallerymodel == null)
            {
                return HttpNotFound();
            }
            return View(gallerymodel);
        }

        // POST: /Gallery/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include="ID,ImageUrl")] GalleryModel gallerymodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gallerymodel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(gallerymodel);
        }

        // GET: /Gallery/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GalleryModel gallerymodel = await db.Images.FindAsync(id);
            if (gallerymodel == null)
            {
                return HttpNotFound();
            }
            return View(gallerymodel);
        }

        // POST: /Gallery/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            GalleryModel gallerymodel = await db.Images.FindAsync(id);
            db.Images.Remove(gallerymodel);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
