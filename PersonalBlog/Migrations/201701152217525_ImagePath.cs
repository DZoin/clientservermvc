namespace PersonalBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImagePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogPostModels", "ImagePath", c => c.String());
            DropColumn("dbo.BlogPostModels", "Image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BlogPostModels", "Image", c => c.Binary());
            DropColumn("dbo.BlogPostModels", "ImagePath");
        }
    }
}
