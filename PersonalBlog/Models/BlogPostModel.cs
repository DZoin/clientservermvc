﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace PersonalBlog.Models
{
    public class BlogPostModel
    {
        public int ID { get; set; }
        public String ImagePath { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Title cannot be more than 50 characters long")]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Display(Name = "Date added")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateAdded { get; set; }
    }

    public class BlogPostDbContext : DbContext
    {
        public DbSet<BlogPostModel> BlogPost { get; set; } 
    }

}