﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PersonalBlog.Models
{
    public class GalleryModel
    {
        public int ID { get; set; }
        [Required]
        public string ImageUrl { get; set; }
    }

    public class GalleryModelDbContext : DbContext
    {
        public DbSet<GalleryModel> Images { get; set; }
    }
}